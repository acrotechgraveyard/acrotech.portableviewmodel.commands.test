﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class CancelCommandArgsTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            var args = new CancelCommandArgs();
            Assert.AreEqual(false, args.IsCancelRequested);

            args = new CancelCommandArgs(true);
            Assert.AreEqual(true, args.IsCancelRequested);
        }
    }
}
