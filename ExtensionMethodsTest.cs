﻿// disable warning about how TestCommand.CanExecuteChanged is never used
#pragma warning disable 0067

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Input;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class ExtensionMethodsTest
    {
        [TestMethod]
        public void CanExecuteTest()
        {
            var cmd = new TestCommand();

            cmd.CanExecute();

            Assert.IsTrue(cmd.CanExecuteCalled);
        }

        [TestMethod]
        public void ExecuteTest()
        {
            var cmd = new TestCommand();

            cmd.Execute();

            Assert.IsTrue(cmd.ExecuteCalled);
        }

        class TestCommand : ICommand
        {
            public bool CanExecuteCalled { get; private set; }
            public bool ExecuteCalled { get; private set; }

            public bool CanExecute(object parameter)
            {
                CanExecuteCalled = true;

                return true;
            }

            public event EventHandler CanExecuteChanged;

            public void Execute(object parameter)
            {
                ExecuteCalled = true;
            }
        }

    }
}
