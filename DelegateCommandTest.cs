﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class DelegateCommandTest
    {
        public const string TestParameter = "Test";

        [TestMethod]
        public void NullExecuteActionTest()
        {
            var cmd = new DelegateCommand<object>(null);

            // ensure we don't throw an exception here
            cmd.Execute();
        }

        [TestMethod]
        public void NullCanExecuteActionTest()
        {
            var cmd = new DelegateCommand<object>(null, null);

            Assert.IsTrue(cmd.CanExecute());
        }

        [TestMethod]
        public void NonNullExecuteActionWithNullParameterTest()
        {
            var actionExecuted = false;
            object executionParameter = null;

            var cmd = new DelegateCommand<object>(x => { actionExecuted = true; executionParameter = x; });

            cmd.Execute();

            Assert.IsTrue(actionExecuted);
            Assert.IsNull(executionParameter);
        }

        [TestMethod]
        public void NonNullExecuteActionWithNonNullParameterTest()
        {
            var actionExecuted = false;
            object executionParameter = null;

            var cmd = new DelegateCommand<object>(x => { actionExecuted = true; executionParameter = x; });

            cmd.Execute(TestParameter);

            Assert.IsTrue(actionExecuted);
            Assert.AreEqual(TestParameter, executionParameter);
        }

        [TestMethod]
        public void GenericTypedNonNullExecuteActionWithNullParameterTest()
        {
            var actionExecuted = false;
            object executionParameter = null;

            var cmd = new DelegateCommand<string>(x => { actionExecuted = true; executionParameter = x; });

            cmd.Execute();

            Assert.IsTrue(actionExecuted);
            Assert.IsNull(executionParameter);
        }

        [TestMethod]
        public void GenericTypedNonNullExecuteActionWithNonNullParameterTest()
        {
            var actionExecuted = false;
            object executionParameter = null;

            var cmd = new DelegateCommand<string>(x => { actionExecuted = true; executionParameter = x; });

            cmd.Execute(TestParameter);

            Assert.IsTrue(actionExecuted);
            Assert.AreEqual(TestParameter, executionParameter);
        }

        [TestMethod]
        public void NonNullCanExecuteActionWithNullParameterTest()
        {
            var actionExecuted = false;
            object executionParameter = null;

            var cmd = new DelegateCommand<object>(null, x => { actionExecuted = true; executionParameter = x; return true; });

            Assert.IsTrue(cmd.CanExecute());

            Assert.IsTrue(actionExecuted);
            Assert.IsNull(executionParameter);
        }

        [TestMethod]
        public void NonNullCanExecuteActionWithNonNullParameterTest()
        {
            var actionExecuted = false;
            object executionParameter = null;

            var cmd = new DelegateCommand<object>(null, x => { actionExecuted = true; executionParameter = x; return x.ToString() == TestParameter; });

            Assert.IsTrue(cmd.CanExecute(TestParameter));

            Assert.IsTrue(actionExecuted);
            Assert.AreEqual(TestParameter, executionParameter);

            actionExecuted = false;
            executionParameter = null;

            Assert.IsFalse(cmd.CanExecute(TestParameter + "."));

            Assert.IsTrue(actionExecuted);
            Assert.AreEqual(TestParameter + ".", executionParameter);
        }

        [TestMethod]
        public void GenericTypedNonNullCanExecuteActionWithNullParameterTest()
        {
            var actionExecuted = false;
            object executionParameter = null;

            var cmd = new DelegateCommand<string>(null, x => { actionExecuted = true; executionParameter = x; return true; });

            Assert.IsTrue(cmd.CanExecute());

            Assert.IsTrue(actionExecuted);
            Assert.IsNull(executionParameter);
        }

        [TestMethod]
        public void GenericTypedNonNullCanExecuteActionWithNonNullParameterTest()
        {
            var actionExecuted = false;
            object executionParameter = null;

            var cmd = new DelegateCommand<string>(null, x => { actionExecuted = true; executionParameter = x; return x == TestParameter; });

            Assert.IsTrue(cmd.CanExecute(TestParameter));

            Assert.IsTrue(actionExecuted);
            Assert.AreEqual(TestParameter, executionParameter);

            actionExecuted = false;
            executionParameter = null;

            Assert.IsFalse(cmd.CanExecute(TestParameter + "."));

            Assert.IsTrue(actionExecuted);
            Assert.AreEqual(TestParameter + ".", executionParameter);
        }

        [TestMethod]
        public void RaiseCanExecuteChangedWithNullHandlerTest()
        {
            var cmd = new TestDelegateCommand();

            // ensure we don't throw an exception here
            cmd.PerformRaiseCanExecuteChanged(null);
        }

        [TestMethod]
        public void RaiseCanExecuteChangedWithNonNullHandlerTest()
        {
            var cmd = new TestDelegateCommand();

            var eventHandled = false;
            EventHandler handler = (s, e) => eventHandled = true;

            cmd.PerformRaiseCanExecuteChanged(handler);

            Assert.IsTrue(eventHandled);
        }

        [TestMethod]
        public void RaiseCanExecuteChangedTest()
        {
            var cmd = new DelegateCommand<object>(null);

            var eventHandled = false;

            cmd.CanExecuteChanged += (s, e) => eventHandled = true;

            cmd.RaiseCanExecuteChanged();

            Assert.IsTrue(eventHandled);
        }

        [TestMethod]
        public void NonGenericNullExecuteParameterlessActionTest()
        {
            var cmd = new DelegateCommand((Action)null);

            Assert.IsNull(cmd.ExecuteAction);
        }

        [TestMethod]
        public void NonGenericNullExecuteParameterActionTest()
        {
            var cmd = new DelegateCommand((Action<object>)null);

            Assert.IsNull(cmd.ExecuteAction);
        }

        [TestMethod]
        public void NonGenericNonNullExecuteParameterlessActionTest()
        {
            var actionExecuted = false;

            Action action = () => actionExecuted = true;

            var cmd = new DelegateCommand(action);

            Assert.IsNotNull(cmd.ExecuteAction);

            cmd.ExecuteAction(null);

            Assert.IsTrue(actionExecuted);
        }

        [TestMethod]
        public void NonGenericNonNullExecuteParameterActionTest()
        {
            var actionExecuted = false;
            object actionParameter = null;

            Action<object> action = x => { actionExecuted = true; actionParameter = x; };

            var cmd = new DelegateCommand(action);

            Assert.IsNotNull(cmd.ExecuteAction);

            cmd.ExecuteAction(TestParameter);

            Assert.IsTrue(actionExecuted);
            Assert.AreEqual(TestParameter, actionParameter.ToString());
        }

        [TestMethod]
        public void NonGenericNullCanExecuteFuncTest()
        {
            var cmd = new DelegateCommand((Action)null, (Func<bool>)null);

            Assert.IsNull(cmd.CanExecuteAction);
        }

        [TestMethod]
        public void NonGenericNullCanExecutePredicateTest()
        {
            var cmd = new DelegateCommand((Action<object>)null, (Predicate<object>)null);

            Assert.IsNull(cmd.CanExecuteAction);
        }

        [TestMethod]
        public void NonGenericNonNullCanExecuteFuncTest()
        {
            var actionExecuted = false;

            Func<bool> action = () => { actionExecuted = true; return true; };

            var cmd = new DelegateCommand(null, action);

            Assert.IsNotNull(cmd.CanExecuteAction);

            Assert.IsTrue(cmd.CanExecuteAction(null));

            Assert.IsTrue(actionExecuted);
        }

        [TestMethod]
        public void NonGenericNonNullCanExecutePredicateTest()
        {
            var actionExecuted = false;
            object actionParameter = null;

            Predicate<object> action = x => { actionExecuted = true; actionParameter = x; return x.ToString() == TestParameter; };

            var cmd = new DelegateCommand(null, action);

            Assert.IsNotNull(cmd.CanExecuteAction);

            Assert.IsTrue(cmd.CanExecuteAction(TestParameter));

            Assert.IsTrue(actionExecuted);
            Assert.AreEqual(TestParameter, actionParameter.ToString());

            actionExecuted = false;
            actionParameter = null;

            Assert.IsFalse(cmd.CanExecuteAction(TestParameter + "."));

            Assert.IsTrue(actionExecuted);
            Assert.AreEqual(TestParameter + ".", actionParameter.ToString());
        }

        class TestDelegateCommand : DelegateCommand
        {
            public TestDelegateCommand()
                : base((Action)null)
            {
            }

            public void PerformRaiseCanExecuteChanged(EventHandler canExecuteChanged)
            {
                RaiseCanExecuteChanged(canExecuteChanged);
            }
        }
    }
}
